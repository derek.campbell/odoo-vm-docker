

apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" | sudo tee /etc/apt/sources.list.d/docker.list

apt-get update

apt-get install -y docker-engine

docker create -e POSTGRES_USER=odoo -e POSTGRES_PASSWORD=odoo --name db postgres

docker create -p 0.0.0.0:8069:8069 --name odoo --link db:db -t odoo


